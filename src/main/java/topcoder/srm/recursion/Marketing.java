package topcoder.srm.recursion;

import java.util.Arrays;

/**
 *
 * @author Simon Leghissa
 */
public class Marketing {

    public long howMany(String[] compete) {
        if (compete.length == 0) {
            return 0;
        }
        final String targets[] = new String[compete.length];
        long result = _recursive(0, targets, compete);
        return result != 0 ? result : -1;
    }

    private long _recursive(int i, String[] targets, String[] compete) {
        boolean competesWithTeenager = false;
        boolean competesWithAdult = false;
        if (targets[i] != null) {
            if (targets[i].equals("A")) {
                competesWithTeenager = true;
            } else {
                competesWithAdult = true;
            }
        }
        if (compete[i].trim().length() > 0) {
            for (String s : compete[i].split(" ")) {
                int index = Integer.valueOf(s);
                if (targets[index] != null && targets[index].equals("A")) {
                    competesWithAdult = true;
                }
                if (targets[index] != null && targets[index].equals("T")) {
                    competesWithTeenager = true;
                }
            }
        }
        if (i == targets.length - 1) {
            return (competesWithAdult ? 0 : 1) + (competesWithTeenager ? 0 : 1);
        }
        long countForAdult = 0;
        if (!competesWithTeenager) {
            final String[] copy1 = Arrays.copyOf(targets, targets.length);
            copy1[i] = "T";
            if (compete[i].trim().length() > 0) {
                for (String s : compete[i].split(" ")) {
                    int index = Integer.valueOf(s);
                    copy1[index] = "A";
                }
            }
            countForAdult = _recursive(i + 1, copy1, compete);
        }
        long countForTeenager = 0;
        if (!competesWithAdult) {
            final String[] copy2 = Arrays.copyOf(targets, targets.length);
            copy2[i] = "A";
            if (compete[i].trim().length() > 0) {
                for (String s : compete[i].split(" ")) {
                    int index = Integer.valueOf(s);
                    copy2[index] = "T";
                }
            }
            countForTeenager = _recursive(i + 1, copy2, compete);
        }
        return countForAdult + countForTeenager;
    }
}
