package topcoder.srm.brute_force;

/**
 * Created by Simon Leghissa on 2/2/14.
 */
public class Cafeteria {

    public String latestTime(int[] offset, int[] walkingTime, int[] drivingTime) {
        int time = 14 * 60 + 30;
        int latestTime = 0;
        for (int i = 0; i < offset.length; i++) {
            int latestBusStart = time - drivingTime[i];
            int latestRealBusStart = findLatestStartBefore(latestBusStart, offset[i]);
            latestTime = Math.max(latestTime, latestRealBusStart - walkingTime[i]);
        }
        return format(latestTime);
    }

    private String format(int latestTime) {
        int hours = latestTime / 60;
        hours = hours > 12 ? hours % 12 : hours;
        int minutes = latestTime % 60;
        return String.format("%02d:%02d", hours, minutes);
    }

    private int findLatestStartBefore(int latestBusStart, int offset) {
        while (true) {
            if (latestBusStart % 10 == offset) {
                return latestBusStart;
            }
            latestBusStart--;
        }
    }

}
