package topcoder.srm.brute_force;

import java.util.*;

/**
 * Created by Simon Leghissa on 2/3/14.
 */
public class WordFind {

    public String[] findWords(String[] grid, String[] wordList) {
        final String[] result = new String[wordList.length];
        final Map<String, List<Location>> positionsByWord = new HashMap<>();
        for (String word : wordList) {
            positionsByWord.put(word, new ArrayList<Location>());
        }
        for (int r = 0; r < grid.length; r++) {
            for (int c = 0; c < grid[0].length(); c++) {
                addWords(getHorizontal(grid, r, c), positionsByWord, r, c);
                addWords(getVertical(grid, r, c), positionsByWord, r, c);
                addWords(getDiagonal(grid, r, c), positionsByWord, r, c);
            }
        }
        for (int i = 0; i < wordList.length; i++) {
            final List<Location> locations = positionsByWord.get(wordList[i]);
            if (!locations.isEmpty()) {
                Collections.sort(locations);
                result[i] = locations.get(0).toString();
            } else {
                result[i] = "";
            }
        }
        return result;
    }

    private void addWords(List<String> words, Map<String, List<Location>> positionsByWord, int r, int c) {
        for (String word : words) {
            if (positionsByWord.containsKey(word)) {
                positionsByWord.get(word).add(new Location(r, c));
            }
        }
    }

    private List<String> getDiagonal(String[] grid, int r, int c) {
        final List<String> words = new ArrayList<>();
        final StringBuffer buff = new StringBuffer();
        for (int i = 0; i < Math.min(grid.length - r, grid[0].length() - c); i++) {
            buff.append(grid[r + i].charAt(c + i));
            words.add(buff.toString());
        }
        return words;
    }

    private List<String> getVertical(String[] grid, int r, int c) {
        final List<String> words = new ArrayList<>();
        final StringBuffer buff = new StringBuffer();
        for (int i = r; i < grid.length; i++) {
            buff.append(grid[i].charAt(c));
            words.add(buff.toString());
        }
        return words;
    }

    private List<String> getHorizontal(String[] grid, int r, int c) {
        final List<String> words = new ArrayList<>();
        final StringBuffer buff = new StringBuffer();
        for (int i = c; i < grid[0].length(); i++) {
            buff.append(grid[r].charAt(i));
            words.add(buff.toString());
        }
        return words;
    }

    private class Location implements Comparable<Location> {
        final int r, c;

        private Location(int r, int c) {
            this.r = r;
            this.c = c;
        }

        @Override
        public int compareTo(Location o) {
            int res = r - o.r;
            return res != 0 ? res : c - o.c;
        }

        @Override
        public String toString() {
            return r + " " + c;
        }
    }
}
