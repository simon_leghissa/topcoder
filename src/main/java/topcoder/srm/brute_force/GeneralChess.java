package topcoder.srm.brute_force;

import java.util.*;

/**
 * Created by Simon Leghissa on 2/2/14.
 */
public class GeneralChess {

    final static int[][] moves = new int[][]{new int[]{2, 1}, new int[]{2, -1}, new int[]{1, 2}, new int[]{1, -2}, new int[]{-2, 1}, new int[]{-2, -1}, new int[]{-1, 2}, new int[]{-1, -2}};

    public String[] attackPositions(String[] pieces) {
        final Cell[] piecesCoords = parsePieces(pieces);


        final Map<Cell, List<Cell>> attackPostionsByCell = new HashMap<>();
        for (Cell piece : piecesCoords) {
            final List<Cell> attackPositions = getAttackPositions(piece);
            attackPostionsByCell.put(piece, attackPositions);
        }

        final List<Cell> commonPositions = new ArrayList<>();
        for (Cell cell : attackPostionsByCell.get(piecesCoords[0])) {
            boolean threatensAll = true;
            for (List<Cell> cells : attackPostionsByCell.values()) {
                if (!cells.contains(cell)) {
                    threatensAll = false;
                    break;
                }
            }
            if (threatensAll) {
                commonPositions.add(cell);
            }
        }
        return format(commonPositions);
    }

    private String[] format(List<Cell> commonPositions) {
        Collections.sort(commonPositions);
        final String[] result = new String[commonPositions.size()];
        for (int i = 0; i < commonPositions.size(); i++) {
            result[i] = String.format("%d,%d", commonPositions.get(i).x, commonPositions.get(i).y);
        }
        return result;
    }

    private List<Cell> getAttackPositions(Cell piece) {
        final List<Cell> positions = new ArrayList<>();
        for (int[] move : moves) {
            final int newX = piece.x + move[0];
            final int newY = piece.y + move[1];
            if (newX >= -10000 && newX <= 10000 && newY >= -10000 && newY <= 10000) {
                positions.add(new Cell(newX, newY));
            }
        }
        return positions;
    }

    public static class Cell implements Comparable<Cell> {

        final int x, y;

        public Cell(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Cell cell = (Cell) o;

            if (x != cell.x) return false;
            if (y != cell.y) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }

        @Override
        public int compareTo(Cell o) {
            int result = x - o.x;
            if (result != 0) {
                return result;
            }
            return y - o.y;
        }
    }

    private Cell[] parsePieces(String[] pieces) {
        final Cell[] coords = new Cell[pieces.length];
        for (int i = 0; i < pieces.length; i++) {
            final String[] split = pieces[i].split(",");
            coords[i] = new Cell(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
        }
        return coords;
    }
}
