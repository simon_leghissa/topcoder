package topcoder.srm.straight_forward;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Simon Leghissa on 1/26/14.
 */
public class MedalTable {

    private static enum Medal{
        GOLD(0), SILVER(1), BRONZE(2);

        private final int value;

        Medal(int i) {
            value = i;
        }

        public int getValue(){
            return value;
        }
    }

    public String[] generate(String[] results){
        CountryMedals[] parsedResults = parse(results);
        Arrays.sort(parsedResults);
        return toStrings(parsedResults);
    }

    private CountryMedals[] parse(String[] results){
        final Map<String, CountryMedals> medalsByCountry = new HashMap<>();
        for (String result : results) {
            final String[] splitted = result.split("\\s");
            for (Medal medal : Medal.values()) {
                update(medalsByCountry, medal, splitted[medal.getValue()]);
            }
        }
        return medalsByCountry.values().toArray(new CountryMedals[medalsByCountry.size()]);
    }

    private void update(Map<String, CountryMedals> medalsByCountry, Medal medal, String country){
        if(!medalsByCountry.containsKey(country)){
            medalsByCountry.put(country, new CountryMedals(country));
        }
        medalsByCountry.get(country).getMedals()[medal.getValue()]++;
    }

    private String[] toStrings(CountryMedals[] parsedResults) {
        final String[] result = new String[parsedResults.length];
        for (int i=0; i<parsedResults.length; i++) {
            result[i] = parsedResults[i].toString();
        }
        return result;
    }


    private static class CountryMedals implements Comparable<CountryMedals>{

        private String name;
        private int[] medals;

        private CountryMedals(String name) {
            this.name = name;
            medals = new int[3];
        }

        public String getName() {
            return name;
        }

        public int[] getMedals() {
            return medals;
        }

        @Override
        public String toString() {
            return String.format("%s %d %d %d",
                    name,
                    medals[Medal.GOLD.getValue()],
                    medals[Medal.SILVER.getValue()],
                    medals[Medal.BRONZE.getValue()]
            );
        }

        @Override
        public int compareTo(CountryMedals o) {
            for(int i=0; i<medals.length; i++){
                int compare = o.getMedals()[i] - medals[i];
                if(compare != 0){
                    return compare;
                }
            }
            return name.compareTo(o.getName());
        }
    }
}
