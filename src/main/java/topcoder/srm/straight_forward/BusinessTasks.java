package topcoder.srm.straight_forward;

/**
 * Created by Leghissa Simon on 1/25/14.
 */
public class BusinessTasks {

    public String getTask(String[] list, int n) {
        while (list.length > 1) {
            int i = (n - 1) % list.length;
            list = deleteAt(list, i);
        }
        return list[0];
    }

    private String[] deleteAt(String[] list, int i) {
        final String[] newList = new String[list.length - 1];
        int j = 0;
        for (int k = i + 1; k < list.length; k++) {
            newList[j++] = list[k];
        }
        for (int k = 0; k < i; k++) {
            newList[j++] = list[k];
        }
        return newList;
    }

}
