package topcoder.srm.straight_forward;

/**
 * Created by Simon Leghissa on 1/26/14.
 */
public class TallPeople {

    public int[] getPeople(String[] people) {
        int tallestOfTheShortest = 0;
        int columnTallest[] = null;
        for (String rowString : people) {
            int[] row = toInt(rowString);
            if (columnTallest == null) {
                columnTallest = new int[row.length];
            }
            int shortest = Integer.MAX_VALUE;
            for (int i = 0; i < row.length; i++) {
                shortest = Math.min(row[i], shortest);
                columnTallest[i] = Math.max(columnTallest[i], row[i]);
            }
            tallestOfTheShortest = Math.max(tallestOfTheShortest, shortest);
        }

        return new int[]{tallestOfTheShortest, min(columnTallest)};
    }

    private int min(int[] arr) {
        int min = Integer.MAX_VALUE;
        for (int i : arr) {
            min = Math.min(min, i);
        }
        return min;
    }

    private int[] toInt(String row) {
        String[] heightsAsStrings = row.split("\\s");
        int[] heights = new int[heightsAsStrings.length];
        for (int i = 0; i < heights.length; i++) {
            heights[i] = Integer.parseInt(heightsAsStrings[i]);
        }
        return heights;
    }
}
