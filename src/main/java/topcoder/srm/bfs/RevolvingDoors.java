package topcoder.srm.bfs;

import java.util.Arrays;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * Created by Simon on Leghissa on 1/27/14.
 */
public class RevolvingDoors {

    private static int X = 0;
    private static int Y = 1;

    final static int[][] MOVES = new int[][]{new int[]{0, 1}, new int[]{0, -1}, new int[]{1, 0}, new int[]{-1, 0}};

    public int turns(String[] mapStr) {
        final ParsedMap map = parse(mapStr);
        final int[][] doorIndex = map.doorIndex;

        final Set<Status> visited = new HashSet<>();
        final PriorityQueue<Status> queue = new PriorityQueue<>();
        queue.add(new Status(0, map.start, map.doorStatus, map.map));
        visited.add(queue.peek());
        while (!queue.isEmpty()) {
            final Status current = queue.poll();
            if (Arrays.equals(current.position, map.end)) {
                return current.moves;
            }

            final Set<Status> next = getNextStatuses(current, doorIndex);
            for (Status status : next) {
                if (visited.contains(status)) {
                    continue;
                }
                visited.add(status);
                queue.add(status);
            }
        }
        return -1;
    }

    private Set<Status> getNextStatuses(Status current, int[][] doorIndex) {
        final Set<Status> next = new HashSet<>();
        for (int[] move : MOVES) {
            int[] newPos = new int[]{current.position[0] + move[0], current.position[1] + move[1]};

            final Status newStatus = new Status(current.moves,
                    newPos,
                    current.doors,
                    current.map);
            if (!inBounds(newPos[0], newPos[1], current.map.length, current.map[0].length)) {
                continue;
            }
            final char cell = current.map[newPos[0]][newPos[1]];
            if (cell == 'O') {
                continue;
            }
            if (cell == '#') {
                continue;
            }
            if (((Arrays.equals(move, new int[]{0, 1}) || Arrays.equals(move, new int[]{0, -1})) && cell == '-')
                    || ((Arrays.equals(move, new int[]{-1, 0}) || Arrays.equals(move, new int[]{1, 0})) && cell == '|')) {
                continue;
            }
            if (cell == '-') {
                newStatus.moves++;
                if (inBounds(newPos[0], newPos[1] + 1, current.map.length, current.map[0].length) && current.map[newPos[0]][newPos[1] + 1] == 'O') {
                    newStatus.doors[doorIndex[newPos[0]][newPos[1] + 1]] = DoorStatus.VERTICAL;
                    newStatus.map[newPos[0]][newPos[1]] = ' ';
                    newStatus.map[newPos[0]][newPos[1] + 2] = ' ';
                    newStatus.map[newPos[0] + 1][newPos[1] + 1] = '|';
                    newStatus.map[newPos[0] - 1][newPos[1] + 1] = '|';
                }
                if (inBounds(newPos[0], newPos[1] - 1, current.map.length, current.map[0].length) && current.map[newPos[0]][newPos[1] - 1] == 'O') {
                    newStatus.doors[doorIndex[newPos[0]][newPos[1] - 1]] = DoorStatus.VERTICAL;
                    newStatus.map[newPos[0]][newPos[1]] = ' ';
                    newStatus.map[newPos[0]][newPos[1] - 2] = ' ';
                    newStatus.map[newPos[0] + 1][newPos[1] - 1] = '|';
                    newStatus.map[newPos[0] - 1][newPos[1] - 1] = '|';
                }
            }
            if (cell == '|') {
                newStatus.moves++;
                if (inBounds(newPos[0] + 1, newPos[1], current.map.length, current.map[0].length) && current.map[newPos[0] + 1][newPos[1]] == 'O') {
                    newStatus.doors[doorIndex[newPos[0] + 1][newPos[1]]] = DoorStatus.HORIZONTAL;
                    newStatus.map[newPos[0]][newPos[1]] = ' ';
                    newStatus.map[newPos[0] + 2][newPos[1]] = ' ';
                    newStatus.map[newPos[0] + 1][newPos[1] - 1] = '-';
                    newStatus.map[newPos[0] + 1][newPos[1] + 1] = '-';
                }
                if (inBounds(newPos[0] - 1, newPos[1], current.map.length, current.map[0].length) && current.map[newPos[0] - 1][newPos[1]] == 'O') {
                    newStatus.doors[doorIndex[newPos[0] - 1][newPos[1]]] = DoorStatus.HORIZONTAL;
                    newStatus.map[newPos[0]][newPos[1]] = ' ';
                    newStatus.map[newPos[0] - 2][newPos[1]] = ' ';
                    newStatus.map[newPos[0] - 1][newPos[1] - 1] = '-';
                    newStatus.map[newPos[0] - 1][newPos[1] + 1] = '-';
                }
            }
            next.add(newStatus);
        }
        return next;

    }

    private ParsedMap parse(String[] mapStr) {
        char[][] map = new char[mapStr.length][mapStr[0].length()];
        int[] start = null;
        int[] end = null;
        int doorsIndex[][] = new int[mapStr.length][mapStr[0].length()];
        DoorStatus[] doorStatus = new DoorStatus[10];
        int doorCount = 0;
        for (int i = 0; i < mapStr.length; i++) {
            for (int j = 0; j < mapStr[i].length(); j++) {
                map[i][j] = mapStr[i].charAt(j);
                if (map[i][j] == 'S') {
                    start = new int[]{i, j};
                }
                if (map[i][j] == 'E') {
                    end = new int[]{i, j};
                }
                if (map[i][j] == 'O') {
                    doorsIndex[i][j] = doorCount;
                    if (inBounds(i, j + 1, mapStr.length, mapStr[0].length()) && map[i][j + 1] == '-') {
                        doorStatus[doorCount] = DoorStatus.HORIZONTAL;
                    }
                    if (inBounds(i, j - 1, mapStr.length, mapStr[0].length()) && map[i][j - 1] == '-') {
                        doorStatus[doorCount] = DoorStatus.HORIZONTAL;
                    }
                    if (inBounds(i + 1, j, mapStr.length, mapStr[0].length()) && map[i + 1][j] == '|') {
                        doorStatus[doorCount] = DoorStatus.VERTICAL;
                    }
                    if (inBounds(i - 1, j, mapStr.length, mapStr[0].length()) && map[i - 1][j] == '|') {
                        doorStatus[doorCount] = DoorStatus.VERTICAL;
                    }
                    doorCount++;
                }
            }
        }
        return new ParsedMap(map, start, end, doorsIndex, doorStatus);
    }

    private boolean inBounds(int i, int j, int lengthI, int lengthJ) {
        return i >= 0 && j >= 0 && i < lengthI && j < lengthJ;
    }

    private static class ParsedMap {

        final char[][] map;
        final int[] start;
        final int[] end;
        final DoorStatus[] doorStatus;
        final int[][] doorIndex;

        public ParsedMap(char[][] map, int[] start, int[] end, int[][] doorsIndex, DoorStatus[] doorStatus) {
            this.map = map;
            this.start = start;
            this.end = end;
            this.doorStatus = doorStatus;
            this.doorIndex = doorsIndex;
        }
    }

    private static enum DoorStatus {
        HORIZONTAL, VERTICAL
    }

    private class Status implements Comparable<Status> {

        int moves;
        final int position[];
        final DoorStatus[] doors;
        final char[][] map;

        private Status(int moves, int[] position, DoorStatus[] doors, char[][] map) {
            this.moves = moves;
            this.position = position;
            this.doors = Arrays.copyOf(doors, doors.length);
            this.map = new char[map.length][map[0].length];
            for (int i = 0; i < map.length; i++) {
                for (int j = 0; j < map[0].length; j++) {
                    this.map[i][j] = map[i][j];
                }
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Status status = (Status) o;

            if (!Arrays.equals(doors, status.doors)) return false;
            if (!Arrays.equals(position, status.position)) return false;
            boolean mapsEqual = true;
            for (int i = 0; i < map.length; i++) {
                if (!Arrays.equals(map[i], status.map[i])) {
                    mapsEqual = false;
                }
            }
            if (!mapsEqual) return false;
            return true;
        }

        @Override
        public int hashCode() {
            int result = Arrays.hashCode(position);
            result = 31 * result + Arrays.hashCode(doors);
            for (char[] chars : map) {
                result = 31 * result + Arrays.hashCode(chars);
            }
            return result;
        }

        @Override
        public int compareTo(Status o) {
            return moves - o.moves;
        }
    }
}
