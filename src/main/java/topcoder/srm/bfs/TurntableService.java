package topcoder.srm.bfs;

import java.util.*;

/**
 * Created by Simon Leghissa on 2/1/14.
 */
public class TurntableService {

    public int calculateTime(String[] favorites) {
        final int[] start = new int[favorites.length];
        for (int i = 0; i < start.length; i++) {
            start[i] = i;
        }

        final Set<Integer>[] fav = parse(favorites);

        final Set<State> visited = new HashSet<>();
        final Queue<State> queue = new PriorityQueue<>();
        queue.add(new State(start, new boolean[favorites.length], 0));
        visited.add(queue.peek());
        while (!queue.isEmpty()) {
            final State current = queue.poll();
            boolean allServed = true;
            for (int i = 0; i < current.served.length; i++) {
                if (!current.served[i]) {
                    allServed = false;
                    break;
                }
            }
            if (allServed) {
                return current.elapsed;
            }
            final List<State> next = getNextStates(fav, current);
            for (State state : next) {
                if (visited.contains(state)) {
                    continue;
                }
                queue.add(state);
                visited.add(state);
            }
        }

        return -1;
    }

    private List<State> getNextStates(Set<Integer>[] fav, State current) {
        final ArrayList<State> next = new ArrayList<>();
        for (int offset = 0; offset < current.entries.length; offset++) {
            int negativeOffset = current.entries.length - offset;
            final int[] newEntries = moveByOffset(current.entries, offset);

            boolean newServed[] = Arrays.copyOf(current.served, current.served.length);
            boolean served = false;
            for (int i = 0; i < current.entries.length; i++) {
                if (isPreferred(i, newEntries, fav) && !current.served[i]) {
                    newServed[i] = true;
                    served = true;
                }
            }
            if (served) {
                int newElapsed = current.elapsed;
                if (offset > 0) {
                    if (offset <= negativeOffset) {
                        newElapsed += offset + 1;
                    } else {
                        newElapsed += negativeOffset + 1;
                    }
                }
                next.add(new State(
                        newEntries,
                        newServed, newElapsed + 15));
            }
        }

        return next;
    }

    private int[] moveByOffset(int[] entries, int offset) {
        int[] newEntries = new int[entries.length];
        for (int i = 0; i < entries.length; i++) {
            int pos = (i + offset) % entries.length;
            newEntries[pos] = entries[i];
        }
        return newEntries;
    }

    private boolean isPreferred(int personIndex, int[] entries, Set<Integer>[] fav) {
        return fav[personIndex].contains(entries[personIndex]);
    }

    private Set<Integer>[] parse(String[] favorites) {
        final Set<Integer>[] parsed = new Set[favorites.length];
        for (int i = 0; i < favorites.length; i++) {
            parsed[i] = new HashSet<>();
            final String[] split = favorites[i].split("\\s");
            for (int j = 0; j < split.length; j++) {
                parsed[i].add(Integer.parseInt(split[j]));
            }
        }
        return parsed;
    }

    public static class State implements Comparable<State> {

        int[] entries;
        boolean served[];
        int elapsed;

        public State(int[] entries, boolean[] served, int elapsed) {
            this.entries = entries;
            this.served = served;
            this.elapsed = elapsed;
        }

        @Override
        public int compareTo(State o) {
            return elapsed - o.elapsed;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            State state = (State) o;

            if (!Arrays.equals(entries, state.entries)) return false;
            if (!Arrays.equals(served, state.served)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = Arrays.hashCode(entries);
            result = 31 * result + Arrays.hashCode(served);
            return result;
        }
    }
}
