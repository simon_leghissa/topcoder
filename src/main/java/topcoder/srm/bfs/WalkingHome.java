package topcoder.srm.bfs;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by Simon Leghissa on 2/1/14.
 */
public class WalkingHome {

    private static final char FREE = '.', HOR_ROAD = '-', VERT_ROAD = '|', INTERSECTION = '*', FENCE = 'F', SCHOOL = 'S', HOME = 'H';

    int fewestCrossings(String[] map) {
        final int[] start = getStart(map);

        final boolean[][] visited = new boolean[map.length][map[0].length()];
        final Queue<State> queue = new PriorityQueue<>();
        queue.add(new State(0, start));
        visited[start[0]][start[1]] = true;
        while (!queue.isEmpty()) {
            final State current = queue.poll();
            if (map[current.point[0]].charAt(current.point[1]) == HOME) {
                return current.crossings;
            }
            final List<State> next = getNextPoints(map, current);
            for (State state : next) {
                if (visited[state.point[0]][state.point[1]]) {
                    continue;
                }
                queue.add(state);
                visited[state.point[0]][state.point[1]] = true;
            }
        }
        return -1;
    }

    private List<State> getNextPoints(String[] map, State current) {
        final List<State> next = new ArrayList<>();
        int[] currentPoint = current.point;
        int crossings = current.crossings;

        int[][] moves = new int[][]{new int[]{0, 1}, new int[]{0, -1}, new int[]{1, 0}, new int[]{-1, 0}};
        for (int[] move : moves) {
            int horMove = move[1];
            int vertMove = move[0];

            int[] nextPoint = new int[]{currentPoint[0] + vertMove, currentPoint[1] + horMove};
            if (isInBounds(nextPoint, map)) {
                switch (map[nextPoint[0]].charAt(nextPoint[1])) {
                    case FREE:
                        next.add(new State(crossings, nextPoint));
                        break;
                    case HOR_ROAD:
                        if (horMove != 0) {
                            break;
                        }
                        while (isInBounds(nextPoint, map) && map[nextPoint[0]].charAt(nextPoint[1]) == HOR_ROAD) {
                            nextPoint[0] += vertMove;
                        }
                        if (isInBounds(nextPoint, map) &&
                                (map[nextPoint[0]].charAt(nextPoint[1]) == FREE || map[nextPoint[0]].charAt(nextPoint[1]) == SCHOOL || map[nextPoint[0]].charAt(nextPoint[1]) == HOME)) {
                            next.add(new State(crossings + 1, nextPoint));
                        }
                        break;
                    case VERT_ROAD:
                        if (vertMove != 0) {
                            break;
                        }
                        while (isInBounds(nextPoint, map) && map[nextPoint[0]].charAt(nextPoint[1]) == VERT_ROAD) {
                            nextPoint[1] += horMove;
                        }
                        if (isInBounds(nextPoint, map) &&
                                (map[nextPoint[0]].charAt(nextPoint[1]) == FREE || map[nextPoint[0]].charAt(nextPoint[1]) == SCHOOL || map[nextPoint[0]].charAt(nextPoint[1]) == HOME)) {
                            next.add(new State(crossings + 1, nextPoint));
                        }
                        break;
                    case SCHOOL:
                        next.add(new State(crossings, nextPoint));
                        break;
                    case HOME:
                        next.add(new State(crossings, nextPoint));
                        break;
                }
            }
        }
        return next;
    }

    private boolean isInBounds(int[] point, String[] map) {
        return point[0] >= 0 && point[1] >= 0 && point[0] < map.length && point[1] < map[0].length();
    }

    public static class State implements Comparable<State> {
        final int crossings;
        final int point[];

        public State(int crossings, int[] point) {
            this.crossings = crossings;
            this.point = point;
        }

        @Override
        public int compareTo(State o) {
            return Integer.compare(crossings, o.crossings);
        }
    }

    private int[] getStart(String[] map) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length(); j++) {
                if (map[i].charAt(j) == SCHOOL) {
                    return new int[]{i, j};
                }
            }
        }
        return null;
    }
}
