package topcoder.srm.bfs;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by Simon Leghissa on 1/26/14.
 */
public class SmartWordToy {

    public int minPresses(String start, String finish, String[] forbid) {
        final Pattern forbidPattern = createPattern(forbid);

        final Set<String> visited = new HashSet<String>();
        Queue<QueueItem> queue = new LinkedList<>();
        queue.add(new QueueItem(start, 0));
        visited.add(start);

        while (!queue.isEmpty()) {
            final QueueItem item = queue.poll();
            if (item.getWord().equals(finish)) {
                return item.getPresses();
            }
            final Collection<QueueItem> nextWords = createNextWords(item, forbidPattern);
            for (QueueItem nextWord : nextWords) {
                if (visited.contains(nextWord.getWord())) {
                    continue;
                }
                queue.add(nextWord);
                visited.add(nextWord.getWord());
            }
        }
        return -1;
    }

    protected Pattern createPattern(String[] forbid) {
        final Set<Object> distinctForbids = new HashSet<>();
        final StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < forbid.length; i++) {
            if (distinctForbids.contains(forbid[i])) {
                continue;
            }
            distinctForbids.add(forbid[i]);
            final String[] split = forbid[i].split("\\s");
            if (i != 0) {
                buffer.append("|");
            }
            buffer.append(patternForSingleString(split));
        }
        return Pattern.compile(buffer.toString());
    }

    private String patternForSingleString(String[] split) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("(");
        for (int j = 0; j < split.length; j++) {
            buffer.append("[");
            final char[] charsForIndex = split[j].toCharArray();
            for (int k = 0; k < charsForIndex.length; k++) {
                if (k != 0) {
                    buffer.append("|");
                }
                buffer.append(charsForIndex[k]);
            }
            buffer.append(split[j]);
            buffer.append("]");
        }
        buffer.append(")");
        return buffer.toString();
    }

    protected Collection<QueueItem> createNextWords(QueueItem item, Pattern forbid) {
        final String currentWord = item.getWord();
        final LinkedList<QueueItem> newItems = new LinkedList<>();
        for (int i = 0; i < currentWord.length(); i++) {
            final String nextForward = nextWord(currentWord, i, true);
            if (!forbid.matcher(nextForward).matches()) {
                newItems.add(new QueueItem(nextForward, item.getPresses() + 1));
            }
            final String nextBackwards = nextWord(currentWord, i, false);
            if (!forbid.matcher(nextBackwards).matches()) {
                newItems.add(new QueueItem(nextBackwards, item.getPresses() + 1));
            }
        }
        return newItems;
    }

    protected String nextWord(String word, int charIndex, boolean next) {
        char[] chars = word.toCharArray();
        int nextIndex = chars[charIndex] + (next ? 1 : -1);
        if (nextIndex == 96) {
            nextIndex = 122;
        }
        if (nextIndex == 123) {
            nextIndex = 97;
        }
        chars[charIndex] = (char) (nextIndex);
        return String.copyValueOf(chars);
    }

    protected static class QueueItem {

        private String word;
        private int presses;

        protected QueueItem(String word, Integer presses) {
            this.word = word;
            this.presses = presses;
        }

        public String getWord() {
            return word;
        }

        public int getPresses() {
            return presses;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            QueueItem item = (QueueItem) o;

            if (!word.equals(item.word)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return word.hashCode();
        }
    }
}
