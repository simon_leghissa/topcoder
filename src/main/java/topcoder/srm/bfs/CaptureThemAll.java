package topcoder.srm.bfs;

import java.util.*;

/**
 * Created by Simon Leghissa on 1/26/14.
 */
public class CaptureThemAll {

    public int fastKnight(String knight, String rook, String queen) {
        final Cell knightCell = parsePosition(knight);
        final Cell rookCell = parsePosition(rook);
        final Cell queenCell = parsePosition(queen);

        final Queue<ReachedCell> queue = new LinkedList<>();
        queue.add(new ReachedCell(knightCell, 0, false, false));
        final Set<ReachedCell> visited = new HashSet<>();
        visited.add(queue.peek());
        while (!queue.isEmpty()) {
            final ReachedCell current = queue.poll();
            final Cell currentCell = current.cell;
            if (currentCell.equals(rookCell)) {
                current.reachedRook = true;
            }
            if (currentCell.equals(queenCell)) {
                current.reachedQueen = true;
            }
            if (current.reachedQueen && current.reachedRook) {
                return current.moves;
            }
            final Collection<ReachedCell> reachable = getNextReachable(current, visited);
            queue.addAll(reachable);
            visited.addAll(reachable);
        }
        return -1;
    }

    private Collection<ReachedCell> getNextReachable(ReachedCell current, Set<ReachedCell> visited) {
        int[] colMoves = new int[]{-1, 1};
        int[] rowMoves = new int[]{-2, 2};
        final List<ReachedCell> reachable = new LinkedList<>();
        addForMoves(reachable, colMoves, rowMoves, current, visited);
        addForMoves(reachable, rowMoves, colMoves, current, visited);
        return reachable;
    }

    private void addForMoves(List<ReachedCell> reachable, int[] colMoves, int[] rowMoves, ReachedCell current, Set<ReachedCell> visited) {
        int currCol = current.cell.column;
        int currRow = current.cell.row;

        for (int colMove : colMoves) {
            for (int rowMove : rowMoves) {
                final Cell cell = new Cell(currCol + colMove, currRow + rowMove);
                if (isInBounds(cell)) {
                    final ReachedCell reachedCell = new ReachedCell(cell, current.moves + 1, current.reachedRook, current.reachedQueen);
                    if (!visited.contains(reachedCell)) {
                        reachable.add(reachedCell);
                    }
                }
            }
        }
    }

    private boolean isInBounds(Cell cell) {
        final int col = cell.column;
        final int row = cell.row;
        return col >= 0 && col < 8 && row >= 0 && row < 8;
    }

    protected Cell parsePosition(String s) {
        return new Cell(
                s.charAt(0) - 'a',
                Integer.parseInt(Character.valueOf(s.charAt(1)).toString()) - 1);
    }

    protected static class ReachedCell {

        Cell cell;
        int moves;
        boolean reachedRook;
        boolean reachedQueen;
        Integer hash;

        public ReachedCell(Cell cell, int moves, boolean reachedRook, boolean reachedQueen) {
            this.cell = cell;
            this.moves = moves;
            this.reachedRook = reachedRook;
            this.reachedQueen = reachedQueen;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ReachedCell that = (ReachedCell) o;

            if (reachedQueen != that.reachedQueen) return false;
            if (reachedRook != that.reachedRook) return false;
            if (!cell.equals(that.cell)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            if (hash == null) {
                hash = cell.hashCode();
                hash = 31 * hash + (reachedRook ? 1 : 0);
                hash = 31 * hash + (reachedQueen ? 1 : 0);
            }
            return hash;
        }
    }

    protected static class Cell {

        int row;
        int column;
        Integer hash = null;

        protected Cell(int column, int row) {
            this.row = row;
            this.column = column;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Cell cell = (Cell) o;

            if (column != cell.column) return false;
            if (row != cell.row) return false;

            return true;
        }

        @Override
        public int hashCode() {
            if (hash == null) {
                hash = row;
                hash = 31 * hash + column;
            }
            return hash;
        }
    }
}
