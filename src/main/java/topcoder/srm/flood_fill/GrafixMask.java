package topcoder.srm.flood_fill;

import java.util.*;

/**
 * Created by Simon Leghissa on 2/2/14.
 */
public class GrafixMask {

    public static final boolean BLOCKED = true;

    public int[] sortedAreas(String[] rectangles) {
        final boolean[][] map = createMap(rectangles);
        int[][] connectedMap = createConnectedMap();
        boolean[][] visited = new boolean[map.length][map[0].length];

        final PriorityQueue<State> queue = new PriorityQueue<>();
        queue.add(new State(0, 0, 0));
        while (!queue.isEmpty()) {
            final State current = queue.poll();
            if (visited[current.c][current.r]) {
                continue;
            }
            visited[current.c][current.r] = true;
            connectedMap[current.c][current.r] = current.area;
            final List<State> next = getNext(current, map, connectedMap);
            for (State state : next) {
                queue.add(state);
            }
        }
        return getResults(connectedMap, map);
    }

    private List<State> getNext(State current, boolean[][] map, int[][] connectedMap) {
        int[][] moves = new int[][]{new int[]{0, 1}, new int[]{0, -1}, new int[]{1, 0}, new int[]{-1, 0}};
        final List<State> next = new ArrayList<>();
        for (int[] move : moves) {
            int nextC = current.c + move[0];
            int nextR = current.r + move[1];
            if (isInBounds(nextC, nextR)) {
                if (map[nextC][nextR] == map[current.c][current.r]) {
                    next.add(new State(nextR, nextC, current.area));
                } else {
                    next.add(new State(nextR, nextC, connectedMap[nextC][nextR]));
                }
            }
        }
        return next;
    }

    public static class State implements Comparable<State> {

        int r, c;
        int area;

        public State(int r, int c, int area) {
            this.r = r;
            this.c = c;
            this.area = area;
        }

        @Override
        public int compareTo(State o) {
            return area - o.area;
        }
    }

    private int[] getResults(int[][] connectedMap, boolean[][] map) {
        final Map<Integer, Integer> countByRoot = new HashMap<>();
        for (int i = 0; i < connectedMap.length; i++) {
            for (int j = 0; j < connectedMap[0].length; j++) {
                if (map[i][j] == BLOCKED) {
                    continue;
                }
                if (!countByRoot.containsKey(connectedMap[i][j])) {
                    countByRoot.put(connectedMap[i][j], 0);
                }
                countByRoot.put(connectedMap[i][j], countByRoot.get(connectedMap[i][j]) + 1);
            }
        }
        List<Integer> values = new ArrayList<>(countByRoot.values());
        Collections.sort(values);
        final int[] result = new int[values.size()];
        for (int i = 0; i < values.size(); i++) {
            result[i] = values.get(i);
        }
        return result;
    }

    private boolean isInBounds(int i, int j) {
        return i >= 0 && j >= 0 && i < 400 && j < 600;
    }

    private int[][] createConnectedMap() {
        final int[][] map = new int[400][600];
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                map[i][j] = i * 600 + j;
            }
        }
        return map;
    }

    private boolean[][] createMap(String[] rectangles) {
        final boolean[][] map = new boolean[400][600];
        for (int i = 0; i < rectangles.length; i++) {
            final String[] split = rectangles[i].split("\\s");
            int r1 = Integer.parseInt(split[0]);
            int c1 = Integer.parseInt(split[1]);
            int r2 = Integer.parseInt(split[2]);
            int c2 = Integer.parseInt(split[3]);
            for (int j = r1; j <= r2; j++) {
                for (int k = c1; k <= c2; k++) {
                    map[j][k] = BLOCKED;
                }
            }
        }
        return map;
    }


}
