package topcoder.srm.dynamic_programming;

/**
 * Created by Simon Leghissa on 2/10/14.
 */
public class BadNeighbours {

    private static final int WITH_FIRST = 0, WITHOUT_FIRST = 1;

    public int maxDonations(int[] donations) {
        int max[][] = new int[donations.length][2];
        max[0][WITH_FIRST] = donations[0];
        max[0][WITHOUT_FIRST] = 0;
        max[1][WITH_FIRST] = 0;
        max[1][WITHOUT_FIRST] = donations[1];
        for (int i = 2; i < donations.length; i++) {
            int currentMax[] = new int[2];
            for (int j = 0; j < i - 1; j++) {
                currentMax[WITH_FIRST] = Math.max(currentMax[WITH_FIRST], max[j][WITH_FIRST]);
                currentMax[WITHOUT_FIRST] = Math.max(currentMax[WITHOUT_FIRST], max[j][WITHOUT_FIRST]);
            }
            max[i][WITH_FIRST] = currentMax[WITH_FIRST] + donations[i];
            max[i][WITHOUT_FIRST] = currentMax[WITHOUT_FIRST] + donations[i];
        }
        return Math.max(max[donations.length - 1][WITHOUT_FIRST], max[donations.length - 2][WITH_FIRST]);
    }
}
