package topcoder.srm.dynamic_programming;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Simon Leghissa on 2/10/14.
 */
public class FlowerGarden {

    public int[] getOrdering(int[] height, int[] bloom, int[] wilt) {
        final Map<Integer, Integer> indexesByHeight = new TreeMap<>();
        for (int i = 0; i < height.length; i++) {
            indexesByHeight.put(height[i], i);
        }
        Arrays.sort(height);
        final int[] ordered = new int[height.length];
        for (int i = 0; i < height.length; i++) {
            ordered[i] = height[i];
            for (int j = i; j > 0; j--) {
                if (canMoveForward(ordered[j], ordered[j - 1], indexesByHeight, bloom, wilt)) {
                    final int currentHeight = ordered[j];
                    ordered[j] = ordered[j - 1];
                    ordered[j - 1] = currentHeight;
                } else {
                    break;
                }
            }
        }
        return ordered;
    }

    private boolean canMoveForward(int currentHeight, int forwardHeight, Map<Integer, Integer> indexesByHeight, int[] bloom, int[] wilt) {
        Integer currentIndex = indexesByHeight.get(currentHeight);
        Integer forwardIndex = indexesByHeight.get(forwardHeight);
        return bloom[currentIndex] > wilt[forwardIndex] || wilt[currentIndex] < bloom[forwardIndex];

    }
}
