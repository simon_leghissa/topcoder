package topcoder.srm.dynamic_programming;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Simon Leghissa on 2/10/14.
 */
public class AvoidRoads {

    final int[][] moves = new int[][]{
            new int[]{1, 0},
            new int[]{-1, 0},
            new int[]{0, 1},
            new int[]{0, -1}
    };

    public long numWays(int width, int height, String[] bad) {
        final Set<Set<Point>> badSet = parseSegments(bad);

        final long distinctWays[][][] = new long[width + 1][height + 1][width + height + 1];
        distinctWays[0][0][0] = 1;
        for (int i = 1; i <= width + height; i++) {
            for (int w = 0; w <= width; w++) {
                for (int h = 0; h <= height; h++) {
                    for (int[] move : moves) {
                        final Set<Point> segment = new HashSet<>();
                        segment.add(new Point(w, h));
                        segment.add(new Point(w + move[0], h + move[1]));
                        if (badSet.contains(segment) || !inBounds(w + move[0], width) || !inBounds(h + move[1], height)) {
                            continue;
                        }
                        distinctWays[w][h][i] += distinctWays[w + move[0]][h + move[1]][i - 1];
                    }
                }
            }
        }

        return distinctWays[width][height][width + height];
    }

    private boolean inBounds(int i, int max) {
        return i >= 0 && i <= max;
    }

    private Set<Set<Point>> parseSegments(String[] bad) {
        final Set<Set<Point>> badSet = new HashSet<>();
        for (String s : bad) {
            final String[] split = s.split("\\s");
            final Set<Point> points = new HashSet<>();
            points.add(new Point(Integer.parseInt(split[0]), Integer.parseInt(split[1])));
            points.add(new Point(Integer.parseInt(split[2]), Integer.parseInt(split[3])));
            badSet.add(points);
        }
        return badSet;
    }

    private class Point {
        final int w, h;

        private Point(int w, int h) {
            this.w = w;
            this.h = h;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Point point = (Point) o;

            if (h != point.h) return false;
            if (w != point.w) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = w;
            result = 31 * result + h;
            return result;
        }
    }
}
