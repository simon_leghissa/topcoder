package topcoder.srm.dynamic_programming;

/**
 * Created by Simon Leghissa on 2/9/14.
 */
public class ChessMetric {

    final static int moves[][] = new int[][]{
            new int[]{0, 1},
            new int[]{0, -1},
            new int[]{1, 0},
            new int[]{-1, 0},
            new int[]{-1, 1},
            new int[]{-1, -1},
            new int[]{1, 1},
            new int[]{1, -1},

            new int[]{2, 1},
            new int[]{2, -1},
            new int[]{-2, 1},
            new int[]{-2, -1},
            new int[]{-1, 2},
            new int[]{-1, -2},
            new int[]{1, 2},
            new int[]{1, -2}
    };

    public long howMany(int size, int[] start, int[] end, int numMoves) {
        final long[][][] countByMoves = new long[size][size][numMoves + 1];
        countByMoves[start[0]][start[1]][0] = 1;
        for (int i = 1; i <= numMoves; i++) {
            for (int r = 0; r < size; r++) {
                for (int c = 0; c < size; c++) {
                    for (int[] move : moves) {
                        final int startRow = r - move[0];
                        final int startCol = c - move[1];
                        if (inBounds(startRow, startCol, size)) {
                            countByMoves[r][c][i] += countByMoves[startRow][startCol][i - 1];
                        }
                    }
                }
            }

        }
        return countByMoves[end[0]][end[1]][numMoves];
    }

    private boolean inBounds(int startRow, int startCol, int size) {
        return startCol >= 0 && startRow >= 0 && startCol < size && startRow < size;
    }

}
