package topcoder.srm.dynamic_programming;

import java.math.BigInteger;

public class Count2Sums {

    public int solution(int A[]) {
        if (A.length == 0) {
            return -1;
        }
        final BigInteger[] sumFromLeft = sumFromLeft(A);
        final BigInteger[] sumFromRight = sumFromRight(A);
        for(int i=0;i<A.length; i++){
            if(sumFromLeft[i].equals(sumFromRight[i])){
                return i;
            }
        }
        return -1;
    }
    
    private BigInteger[] sumFromLeft(int[] A) {
        final BigInteger[] sums = new BigInteger[A.length];
        sums[0]=BigInteger.valueOf(0);
        for(int i=1; i<A.length; i++){
            sums[i] = BigInteger.valueOf(0).add(sums[i-1].add(BigInteger.valueOf(A[i-1])));
        }
        return sums;
    }
    
    private BigInteger[] sumFromRight(int[] A) {
        final BigInteger[] sums = new BigInteger[A.length];
        sums[A.length-1]=BigInteger.valueOf(0);
        for(int i=A.length-2; i>=0; i--){
            sums[i] = BigInteger.valueOf(0).add(sums[i+1].add(BigInteger.valueOf(A[i+1])));
        }
        return sums;
    }
}
