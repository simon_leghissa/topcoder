package topcoder.srm.dynamic_programming;


/**
 * Created by Simon Leghissa on 2/9/14.
 */
public class ZigZag {

    final static int POSITIVE = 0, NEGATIVE = 1;

    public int longestZigZag(int[] sequence) {
        final int[][] longest = new int[sequence.length][2];
        for (int i = 0; i < sequence.length; i++) {
            longest[i][POSITIVE] = 1;
            longest[i][NEGATIVE] = 1;
            for (int j = 0; j < i; j++) {
                int difference = sequence[i] - sequence[j];
                if (difference == 0) {
                    longest[i][NEGATIVE] = Math.max(longest[i][NEGATIVE], longest[j][NEGATIVE]);
                    longest[i][POSITIVE] = Math.max(longest[i][POSITIVE], longest[j][POSITIVE]);
                    continue;
                }
                difference = difference > 0 ? POSITIVE : NEGATIVE;
                longest[i][difference] = Math.max(longest[i][difference], longest[j][difference == POSITIVE ? NEGATIVE : POSITIVE] + 1);
            }
        }

        int max = 0;
        for (int[] seq : longest) {
            max = Math.max(seq[POSITIVE], max);
            max = Math.max(seq[NEGATIVE], max);
        }
        return max;
    }
}
