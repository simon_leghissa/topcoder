package topcoder.srm.dynamic_programming;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 * @author Simon Leghissa
 */
public class MaxApples {

    int count=0;
    
    public int maxApplesDijkstra(int[][] grid) {
        if (grid.length == 0) {
            return 0;
        }
        if (grid[0].length == 0) {
            return 0;
        }
        Node[][] nodes = new Node[grid.length][grid[0].length];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                nodes[i][j] = Node.of(i, j, grid[i][j], false);
            }
        }
        final Queue<Node> q = new PriorityQueue<>();
        q.add(nodes[0][0]);
        nodes[0][0].visited = true;
        nodes[0][0].maxApples = grid[0][0];
        while (!q.isEmpty()) {
            count++;
            final Node head = q.poll();
            int i = head.i;
            int j = head.j;
            if (i == nodes.length - 1 && j == nodes[0].length - 1) {
                break;
            }
            if (head.i + 1 < nodes.length && nodes[i + 1][j].visited == false) {
                q.add(nodes[i + 1][j]);
                nodes[i + 1][j].visited = true;
                nodes[i + 1][j].maxApples = head.maxApples + grid[i + 1][j];
            }
            if (head.j + 1 < nodes[0].length && nodes[i][j + 1].visited == false) {
                q.add(nodes[i][j + 1]);
                nodes[i][j + 1].visited = true;
                nodes[i][j + 1].maxApples = head.maxApples + grid[i][j + 1];
            }
        }
        System.out.println("Count "+count);
        return nodes[nodes.length - 1][nodes[0].length - 1].maxApples;
    }

    public static class Node implements Comparable<Node> {

        public int i;
        public int j;
        public int maxApples = 0;
        public int apples;
        public boolean visited;

        public static Node of(int i, int j, int apples, boolean visited) {
            final Node node = new Node();
            node.i = i;
            node.j = j;
            node.apples = apples;
            node.visited = visited;
            return node;
        }

        @Override
        public int compareTo(Node t) {
            return t.apples - this.apples;
        }
    }
    
    int calculated[][];

    public int maxApplesDynamic(int[][] grid) {
        if (grid.length == 0) {
            return 0;
        }
        if (grid[0].length == 0) {
            return 0;
        }
        calculated = new int[grid.length][grid[0].length];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                calculated[i][j] = -1;
            }
        }
        int maxApples = maxApples(grid, grid.length - 1, grid[0].length - 1);
        System.out.println("Count "+count);
        return maxApples;
    }


    private int maxApples(int[][] grid, int i, int j) {
        count++;
        if (i == 0 && j == 0) {
            return grid[0][0];
        }
        int solFromLeft = j > 0 ? provideCalculated(grid, i, j - 1) : 0;
        int solFromUp = i > 0 ? provideCalculated(grid, i - 1, j) : 0;
        return Math.max(solFromLeft, solFromUp) + grid[i][j];
    }

    private int provideCalculated(int[][] grid, int i, int j) {
        if (calculated[i][j] == -1) {
            calculated[i][j] = maxApples(grid, i, j);
        }
        return calculated[i][j];
    }
}
