package topcoder.srm.backtracking;

import java.util.*;

/**
 * Created by Simon Leghissa on 2/2/14.
 */
public class BridgeCrossing {

    public int minTime(int[] times) {
        final List<Integer> timesList = new ArrayList<>();
        for (int time : times) {
            timesList.add(time);
        }

        final PriorityQueue<State> queue = new PriorityQueue<>();
        final Set<State> visited = new HashSet<>();
        queue.add(new State(0, timesList, Collections.<Integer>emptyList()));
        while (!queue.isEmpty()) {
            final State current = queue.poll();
            if (current.stillToCross.isEmpty()) {
                return current.timeSpent;
            }
            visited.add(current);
            final List<State> next = getNext(current);
            for (State state : next) {
                if (!visited.contains(state)) {
                    queue.add(state);
                }
            }
        }

        return -1;
    }

    private List<State> getNext(State current) {
        final ArrayList<State> next = new ArrayList<>();
        if (current.stillToCross.size() == 1 || current.stillToCross.size() == 2) {
            final Integer time1 = current.stillToCross.get(0);
            final Integer time2 = current.stillToCross.size() == 2 ? current.stillToCross.get(1) : Integer.MIN_VALUE;
            final ArrayList<Integer> newCrossed = new ArrayList<>(current.crossed);
            newCrossed.addAll(current.stillToCross);
            next.add(new State(
                    current.timeSpent + Math.max(time1, time2),
                    Collections.<Integer>emptyList(),
                    newCrossed));
        } else {
            for (int i = 0; i < current.stillToCross.size() - 1; i++) {
                for (int j = i + 1; j < current.stillToCross.size(); j++) {
                    final Integer time1 = current.stillToCross.get(i);
                    final Integer time2 = current.stillToCross.get(j);
                    final List<Integer> newToCross = new ArrayList<>(current.stillToCross);
                    final ArrayList<Integer> newCrossed = new ArrayList<>(current.crossed);
                    newToCross.removeAll(Arrays.asList(time1, time2));
                    newCrossed.add(time1);
                    newCrossed.add(time2);
                    for (Integer goingBack : newCrossed) {
                        final ArrayList<Integer> newCrossed1 = new ArrayList<>(newCrossed);
                        final List<Integer> newToCross1 = new ArrayList<>(newToCross);
                        newCrossed1.remove(goingBack);
                        newToCross1.add(goingBack);
                        next.add(new State(
                                current.timeSpent + Math.max(time1, time2) + goingBack,
                                newToCross1,
                                newCrossed1));
                    }
                }
            }
        }
        return next;
    }

    public static class State implements Comparable<State> {

        final int timeSpent;
        final List<Integer> stillToCross;
        final List<Integer> crossed;

        public State(int timeSpent, List<Integer> stillToCross, List<Integer> crossed) {
            this.timeSpent = timeSpent;
            this.stillToCross = stillToCross;
            this.crossed = crossed;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            State state = (State) o;

            if (!stillToCross.equals(state.stillToCross)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return stillToCross.hashCode();
        }

        @Override
        public int compareTo(State o) {
            return timeSpent - o.timeSpent;
        }
    }

}
