package topcoder.srm.backtracking;


import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by Simon Leghissa on 2/3/14.
 */
public class MNS {

    public int combos(int[] numbers) {
        int combos = 0;
        for (List<Integer> combo : combinations(numbers, new boolean[numbers.length], 0)) {
            if (isMagic(combo)) {
                combos++;
            }
        }

        return combos;
    }

    private boolean isMagic(List<Integer> combo) {
        int sum = -1;
        for (int i = 0; i < 3; i++) {
            int currentSum1 = 0;
            int currentSum2 = 0;
            for (int j = 0; j < 3; j++) {
                currentSum1 += combo.get(toIndex(i, j));
                currentSum2 += combo.get(toIndex(j, i));
            }
            if (sum == -1) {
                sum = currentSum1;
            }
            if (currentSum1 != currentSum2 || sum != currentSum1 || sum != currentSum2) {
                return false;
            }
        }
        return true;
    }

    private int toIndex(int r, int c) {
        return r * 3 + c;
    }

    private Set<List<Integer>> combinations(int[] numbers, boolean[] used, int count) {
        final Set<List<Integer>> flattened = new HashSet<>();
        if (count == numbers.length) {
            flattened.add(new LinkedList<Integer>());
            return flattened;
        }
        for (int i = 0; i < numbers.length; i++) {
            if (used[i]) {
                continue;
            }
            used[i] = true;
            for (List<Integer> comb : combinations(numbers, used, count + 1)) {
                comb.add(numbers[i]);
                flattened.add(comb);
            }
            used[i] = false;
        }
        return flattened;
    }

}
