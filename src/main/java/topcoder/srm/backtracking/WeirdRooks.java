package topcoder.srm.backtracking;

import java.util.*;

/**
 * Created by Simon Leghissa on 2/4/14.
 */
public class WeirdRooks {

    public String describe(int[] cols) {
        int maxRooks = cols.length;
        int maxCols = 0;
        int maxSum = 0;
        for (int col : cols) {
            maxCols = Math.max(maxCols, col);
            maxSum += col;
        }
        final Set<Solution> res = new TreeSet<>();

        final Deque<State> stack = new LinkedList<>();
        stack.push(new State(cols.length - 1, maxRooks, new boolean[maxCols], maxSum));

        while (!stack.isEmpty()) {
            final State current = stack.pop();
            if (current.currentRow < 0) {
                res.add(new Solution(maxRooks - current.missingRooks, current.sum));
                continue;
            }
            //line without rook
            int newSum = current.sum;
            for (int j = 0; j < cols[current.currentRow]; j++) {
                if (current.usedCols[j]) {
                    newSum--;
                }
            }
            stack.push(new State(current.currentRow - 1, current.missingRooks, current.usedCols, newSum));

            if (current.missingRooks > 0) {
                for (int i = 0; i < cols[current.currentRow]; i++) {
                    if (!current.usedCols[i]) {
                        newSum = current.sum;
                        for (int j = 0; j < cols[current.currentRow]; j++) {
                            if (j <= i || current.usedCols[j]) {
                                newSum--;
                            }
                        }
                        final boolean[] newUsed = Arrays.copyOf(current.usedCols, current.usedCols.length);
                        newUsed[i] = true;
                        stack.push(new State(current.currentRow - 1, current.missingRooks - 1, newUsed, newSum));
                    }
                }
            }
        }
        return format(res);
    }

    private String format(Set<Solution> res) {
        final StringBuffer buff = new StringBuffer();
        for (Solution solution : res) {
            buff.append(String.format("%s,%s ", solution.r, solution.f));
        }
        buff.deleteCharAt(buff.length() - 1);
        return buff.toString();
    }

    private static class State {

        int currentRow, missingRooks, sum;
        final boolean[] usedCols;

        private State(int currentRow, int missingRooks, boolean[] usedCols, int sum) {
            this.currentRow = currentRow;
            this.missingRooks = missingRooks;
            this.sum = sum;
            this.usedCols = usedCols;
        }
    }

    private static class Solution implements Comparable<Solution> {
        final int r, f;

        private Solution(int r, int f) {
            this.r = r;
            this.f = f;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Solution solution = (Solution) o;

            if (f != solution.f) return false;
            if (r != solution.r) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = r;
            result = 31 * result + f;
            return result;
        }

        @Override
        public int compareTo(Solution o) {
            int res = r - o.r;
            if (res == 0) {
                res = f - o.f;
            }
            return res;
        }
    }
}
