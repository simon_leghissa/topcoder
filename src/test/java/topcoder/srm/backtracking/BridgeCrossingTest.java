package topcoder.srm.backtracking;


import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 2/2/14.
 */
public class BridgeCrossingTest {

    private BridgeCrossing instance = new BridgeCrossing();

    @Test
    public void example0() {
        final int got = instance.minTime(new int[]{1, 2, 5, 10}
        );
        assertThat(got).isEqualTo(17);
    }

    @Test
    public void example1() {
        final int got = instance.minTime(new int[]{1, 2, 3, 4, 5}
        );
        assertThat(got).isEqualTo(16);
    }

    @Test
    public void example2() {
        final int got = instance.minTime(new int[]{100}
        );
        assertThat(got).isEqualTo(100);
    }

    @Test
    public void example3() {
        final int got = instance.minTime(new int[]{1, 2, 3, 50, 99, 100}
        );
        assertThat(got).isEqualTo(162);
    }
}
