package topcoder.srm.backtracking;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 2/4/14.
 */
public class MNSTest {

    private final MNS instance = new MNS();

    @Test
    public void example0() {
        final int got = instance.combos(new int[]{1, 2, 3, 3, 2, 1, 2, 2, 2});
        assertThat(got).isEqualTo(18);
    }

    @Test
    public void example1() {
        final int got = instance.combos(new int[]{4, 4, 4, 4, 4, 4, 4, 4, 4});
        assertThat(got).isEqualTo(1);
    }

    @Test
    public void example2() {
        final int got = instance.combos(new int[]{1, 5, 1, 2, 5, 6, 2, 3, 2});
        assertThat(got).isEqualTo(36);
    }

    @Test
    public void example3() {
        final int got = instance.combos(new int[]{1, 2, 6, 6, 6, 4, 2, 6, 4});
        assertThat(got).isEqualTo(0);
    }

}
