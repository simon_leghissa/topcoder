package topcoder.srm.brute_force;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 2/3/14.
 */
public class WordFindTest {

    private final WordFind instance = new WordFind();

    @Test
    public void example0() {
        final String[] got = instance.findWords(new String[]{
                "TEST",
                "GOAT",
                "BOAT"
        }, new String[]
                {"GOAT", "BOAT", "TEST"});
        assertThat(got).isEqualTo(new String[]{"1 0",
                "2 0",
                "0 0"});
    }

    @Test
    public void example1() {
        final String[] got = instance.findWords(new String[]{"SXXX",
                "XQXM",
                "XXLA",
                "XXXR"}, new String[]
                {"SQL", "RAM"}
        );
        assertThat(got).isEqualTo(new String[]{"0 0",
                ""});
    }

    @Test
    public void example2() {
        final String[] got = instance.findWords(new String[]{"EASYTOFINDEAGSRVHOTCJYG",
                "FLVENKDHCESOXXXXFAGJKEO",
                "YHEDYNAIRQGIZECGXQLKDBI",
                "DEIJFKABAQSIHSNDLOMYJIN",
                "CKXINIMMNGRNSNRGIWQLWOG",
                "VOFQDROQGCWDKOUYRAFUCDO",
                "PFLXWTYKOITSURQJGEGSPGG"}, new String[]
                {"EASYTOFIND", "DIAG", "GOING", "THISISTOOLONGTOFITINTHISPUZZLE"}
        );
        assertThat(got).isEqualTo(new String[]{"0 0",
                "1 6",
                "0 22",
                ""});
    }
}
