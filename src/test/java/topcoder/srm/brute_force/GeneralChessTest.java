package topcoder.srm.brute_force;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 2/2/14.
 */
public class GeneralChessTest {

    final GeneralChess instance = new GeneralChess();

    @Test
    public void example0() {
        final String[] got = instance.attackPositions(new String[]{"0,0"});
        assertThat(got).isEqualTo(new String[]{"-2,-1", "-2,1", "-1,-2", "-1,2", "1,-2", "1,2", "2,-1", "2,1"});
    }

    @Test
    public void example1() {
        final String[] got = instance.attackPositions(new String[]{"2,1", "-1,-2"});
        assertThat(got).isEqualTo(new String[]{"0,0", "1,-1"});
    }

    @Test
    public void example2() {
        final String[] got = instance.attackPositions(new String[]{"0,0", "2,1"});
        assertThat(got).isEqualTo(new String[]{});
    }

    @Test
    public void example3() {
        final String[] got = instance.attackPositions(new String[]{"-1000,1000", "-999,999", "-999,997"});
        assertThat(got).isEqualTo(new String[]{"-1001,998"});
    }
}
