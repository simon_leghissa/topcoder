package topcoder.srm.recursion;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author Simon Leghissa
 */
public class MarketingTest {
    
    final Marketing instance = new Marketing();
    
    @Test
    public void noProducts() {
        final long got = instance.howMany(new String[0]);
        assertEquals(0, got);
    }
    
    @Test
    public void oneProduct() {
        final long got = instance.howMany(new String[]{""});
        assertEquals(2, got);
    }
    
    @Test
    public void twoProducts() {
        final long got = instance.howMany(new String[]{"", ""});
        assertEquals(4, got);
    }
    
    @Test
    public void twoCompetingProducts() {
        final long got = instance.howMany(new String[]{"1", ""});
        assertEquals(2, got);
    }
    
    @Test
    public void a() {
        final long got = instance.howMany(new String[]{"1 4","2","3","0",""});
        assertEquals(2, got);
    }
    
    @Test
    public void b() {
        final long got = instance.howMany(new String[]{"1","2","0"});
        assertEquals(-1, got);
    }
    
    @Test
    public void c() {
        final long got = instance.howMany(new String[]{"1","2","3","0","0 5","1"});
        assertEquals(2, got);
    }
    
    @Test
    public void d() {
        final long got = instance.howMany(new String[]{"","","","","","","","","","",
 "","","","","","","","","","",
 "","","","","","","","","",""});
        assertEquals(1073741824, got);
    }
    
    @Test
    public void e() {
        final long got = instance.howMany(new String[]{"1","2","3","0","5","6","4"});
        assertEquals(-1, got);
    }
    
}
