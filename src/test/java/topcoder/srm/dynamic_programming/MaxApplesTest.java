package topcoder.srm.dynamic_programming;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author Simon Leghissa
 */
public class MaxApplesTest {

    final MaxApples instance = new MaxApples();

    Random random = new Random();

    @Test
    public void emptyGrid() {
        int[][] grid = new int[0][0];
        final int got = instance.maxApplesDijkstra(grid);
        assertEquals(0, got);
    }

    @Test
    public void gridwithOneRow() {
        int[][] grid = new int[1][];
        grid[0] = new int[]{1, 2};
        final int got = instance.maxApplesDijkstra(grid);
        assertEquals(3, got);
    }

    @Test
    public void gridwithOneColumn() {
        int[][] grid = new int[2][1];
        grid[0] = new int[]{1};
        grid[1] = new int[]{1};
        final int got = instance.maxApplesDijkstra(grid);
        assertEquals(2, got);
    }

    @Test
    public void gridWithMultipleRowsAndColumns() {
        int[][] grid = new int[2][2];
        grid[0] = new int[]{1, 5};
        grid[1] = new int[]{4, 4};
        final int got = instance.maxApplesDijkstra(grid);
        assertEquals(10, got);
    }

    private int[][] randomMap(int n) {
        int[][] arr = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; i < n; i++) {
                arr[i][j] = Math.abs(random.nextInt(1000));
            }
        }
        return arr;
    }

}
