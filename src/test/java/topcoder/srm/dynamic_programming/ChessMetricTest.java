package topcoder.srm.dynamic_programming;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 2/9/14.
 */
public class ChessMetricTest {

    final ChessMetric instance = new ChessMetric();

    @Test
    public void example0() {
        final long got = instance.howMany(3,
                new int[]{0, 0},
                new int[]{1, 0},
                1);

        assertThat(got).isEqualTo(1);
    }

    @Test
    public void example1() {
        final long got = instance.howMany(3,
                new int[]{0, 0},
                new int[]{1, 2},
                1);

        assertThat(got).isEqualTo(1);
    }

    @Test
    public void example2() {
        final long got = instance.howMany(3,
                new int[]{0, 0},
                new int[]{2, 2},
                1);

        assertThat(got).isEqualTo(0);
    }

    @Test
    public void example3() {
        final long got = instance.howMany(3,
                new int[]{0, 0},
                new int[]{0, 0},
                2);

        assertThat(got).isEqualTo(5);
    }

    @Test
    public void example4() {
        final long got = instance.howMany(100,
                new int[]{0, 0},
                new int[]{0, 99},
                50);

        assertThat(got).isEqualTo(243097320072600L);
    }

}
