package topcoder.srm.dynamic_programming;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 2/10/14.
 */
public class BadNeighboursTest {

    final BadNeighbours instance = new BadNeighbours();

    @Test
    public void example0() {
        final int got = instance.maxDonations(new int[]{10, 3, 2, 5, 7, 8});
        assertThat(got).isEqualTo(19);
    }

    @Test
    public void example1() {
        final int got = instance.maxDonations(new int[]{11, 15});
        assertThat(got).isEqualTo(15);
    }

    @Test
    public void example2() {
        final int got = instance.maxDonations(new int[]{7, 7, 7, 7, 7, 7, 7});
        assertThat(got).isEqualTo(21);
    }

    @Test
    public void example3() {
        final int got = instance.maxDonations(new int[]{1, 2, 3, 4, 5, 1, 2, 3, 4, 5});
        assertThat(got).isEqualTo(16);
    }

    @Test
    public void example4() {
        final int got = instance.maxDonations(new int[]{94, 40, 49, 65, 21, 21, 106, 80, 92, 81, 679, 4, 61,
                6, 237, 12, 72, 74, 29, 95, 265, 35, 47, 1, 61, 397,
                52, 72, 37, 51, 1, 81, 45, 435, 7, 36, 57, 86, 81, 72});
        assertThat(got).isEqualTo(2926);
    }
}
