package topcoder.srm.dynamic_programming;

import junit.framework.Assert;
import org.junit.Test;

import java.util.Random;

public class Count2SumsTest {

    final Random random = new Random();

    @Test
    public void specsTestCase() {
        int arr[] = new int[]{-7, 1, 5, 2, -4, 3, 0};
        int solution = new Count2Sums().solution(arr);
        Assert.assertEquals(3, solution);
    }
    
    @Test
    public void largeInputTest() {
        int arr[] = generateRansomArray(10000000);
        new Count2Sums().solution(arr);
    }
    
    @Test
    public void shouldReturnMinusOneIfArrayIsEmpty() {
        int arr[] = new int[0];
        int solution = new Count2Sums().solution(arr);
        Assert.assertEquals(-1, solution);
    }

    private int[] generateRansomArray(int N) {
        int arr[] = new int[N];
        for (int i = 0; i < N; i++) {
            arr[i] = random.nextInt();
        }
        return arr;
    }
}
