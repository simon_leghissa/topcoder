package topcoder.srm.dynamic_programming;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 2/10/14.
 */
public class AvoidRoadsTest {

    final AvoidRoads instance = new AvoidRoads();

    @Test
    public void example0() {
        final long got = instance.numWays(6,
                6,
                new String[]{"0 0 0 1", "6 6 5 6"}
        );
        assertThat(got).isEqualTo(252);
    }

    @Test
    public void example1() {
        final long got = instance.numWays(1,
                1,
                new String[]{}
        );
        assertThat(got).isEqualTo(2);
    }

    @Test
    public void example2() {
        final long got = instance.numWays(35,
                31,
                new String[]{}
        );
        assertThat(got).isEqualTo(6406484391866534976L);
    }

    @Test
    public void example3() {
        final long got = instance.numWays(2,
                2,
                new String[]{"0 0 1 0", "1 2 2 2", "1 1 2 1"}
        );
        assertThat(got).isEqualTo(0);
    }
}
