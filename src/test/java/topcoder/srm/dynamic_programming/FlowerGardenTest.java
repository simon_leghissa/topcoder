package topcoder.srm.dynamic_programming;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 2/10/14.
 */
public class FlowerGardenTest {

    final FlowerGarden instance = new FlowerGarden();

    @Test
    public void example0() {
        final int[] got = instance.getOrdering(
                new int[]{5, 4, 3, 2, 1},
                new int[]{1, 1, 1, 1, 1},
                new int[]{365, 365, 365, 365, 365}
        );
        assertThat(got).isEqualTo(new int[]{1, 2, 3, 4, 5});
    }

    @Test
    public void example1() {
        final int[] got = instance.getOrdering(
                new int[]{5, 4, 3, 2, 1},
                new int[]{1, 5, 10, 15, 20},
                new int[]{4, 9, 14, 19, 24}
        );
        assertThat(got).isEqualTo(new int[]{5, 4, 3, 2, 1});
    }

    @Test
    public void example2() {
        final int[] got = instance.getOrdering(
                new int[]{5, 4, 3, 2, 1},
                new int[]{1, 5, 10, 15, 20},
                new int[]{5, 10, 15, 20, 25}
        );
        assertThat(got).isEqualTo(new int[]{1, 2, 3, 4, 5});
    }

    @Test
    public void example3() {
        final int[] got = instance.getOrdering(
                new int[]{5, 4, 3, 2, 1},
                new int[]{1, 5, 10, 15, 20},
                new int[]{5, 10, 14, 20, 25}
        );
        assertThat(got).isEqualTo(new int[]{3, 4, 5, 1, 2});
    }

    @Test
    public void example4() {
        final int[] got = instance.getOrdering(
                new int[]{1, 2, 3, 4, 5, 6},
                new int[]{1, 3, 1, 3, 1, 3},
                new int[]{2, 4, 2, 4, 2, 4}
        );
        assertThat(got).isEqualTo(new int[]{2, 4, 6, 1, 3, 5});
    }

    @Test
    public void example5() {
        final int[] got = instance.getOrdering(
                new int[]{3, 2, 5, 4},
                new int[]{1, 2, 11, 10},
                new int[]{4, 3, 12, 13}
        );
        assertThat(got).isEqualTo(new int[]{4, 5, 2, 3});
    }
}
