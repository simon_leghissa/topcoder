package topcoder.srm.dynamic_programming;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 2/9/14.
 */
public class ZigZagTest {

    private final ZigZag instance = new ZigZag();

    @Test
    public void example0() {
        final int got = instance.longestZigZag(new int[]{1, 7, 4, 9, 2, 5});
        assertThat(got).isEqualTo(6);
    }

    @Test
    public void example1() {
        final int got = instance.longestZigZag(new int[]{1, 17, 5, 10, 13, 15, 10, 5, 16, 8});
        assertThat(got).isEqualTo(7);
    }

    @Test
    public void example2() {
        final int got = instance.longestZigZag(new int[]{44});
        assertThat(got).isEqualTo(1);
    }

    @Test
    public void example3() {
        final int got = instance.longestZigZag(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9});
        assertThat(got).isEqualTo(2);
    }

    @Test
    public void example4() {
        final int got = instance.longestZigZag(new int[]{70, 55, 13, 2, 99, 2, 80, 80, 80, 80, 100, 19, 7, 5, 5, 5, 1000, 32, 32});
        assertThat(got).isEqualTo(8);
    }

    @Test
    public void example5() {
        final int got = instance.longestZigZag(new int[]{374, 40, 854, 203, 203, 156, 362, 279, 812, 955,
                600, 947, 978, 46, 100, 953, 670, 862, 568, 188,
                67, 669, 810, 704, 52, 861, 49, 640, 370, 908,
                477, 245, 413, 109, 659, 401, 483, 308, 609, 120,
                249, 22, 176, 279, 23, 22, 617, 462, 459, 244});
        assertThat(got).isEqualTo(36);
    }

}
