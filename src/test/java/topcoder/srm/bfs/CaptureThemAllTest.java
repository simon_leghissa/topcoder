package topcoder.srm.bfs;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;
import static topcoder.srm.bfs.CaptureThemAll.Cell;

/**
 * Created by Simon Leghissa on 1/26/14.
 */
public class CaptureThemAllTest {

    final CaptureThemAll instance = new CaptureThemAll();

    @Test
    public void parsePosition() {
        final Cell got = instance.parsePosition("h1");
        assertThat(got).isEqualTo(new Cell(7, 0));
    }

    @Test
    public void example0() {
        final int got = instance.fastKnight("a1",
                "b3",
                "c5"
        );
        assertThat(got).isEqualTo(2);
    }

    @Test
    public void example1() {
        final int got = instance.fastKnight("b1",
                "c3",
                "a3"
        );
        assertThat(got).isEqualTo(3);
    }

    @Test
    public void example2() {
        final int got = instance.fastKnight("a1",
                "a2",
                "b2"
        );
        assertThat(got).isEqualTo(6);
    }

    @Test
    public void example3() {
        final int got = instance.fastKnight("a5",
                "b7",
                "e4"

        );
        assertThat(got).isEqualTo(3);
    }

    @Test
    public void example4() {
        final int got = instance.fastKnight("h8",
                "e2",
                "d2"
        );
        assertThat(got).isEqualTo(6);
    }
}
