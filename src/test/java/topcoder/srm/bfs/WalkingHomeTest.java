package topcoder.srm.bfs;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 2/1/14.
 */
public class WalkingHomeTest {

    private final WalkingHome instance = new WalkingHome();

    @Test
    public void example0() {
        final int got = instance.fewestCrossings(new String[]{
                "S.|..",
                "..|.H"}
        );
        assertThat(got).isEqualTo(1);
    }

    @Test
    public void example1() {
        final int got = instance.fewestCrossings(new String[]{
                "S.|..",
                "..|.H",
                "..|..",
                "....."}
        );
        assertThat(got).isEqualTo(0);
    }

    @Test
    public void example2() {
        final int got = instance.fewestCrossings(new String[]{
                "S.||...",
                "..||...",
                "..||...",
                "..||..H"}
        );
        assertThat(got).isEqualTo(1);
    }

    @Test
    public void example3() {
        final int got = instance.fewestCrossings(new String[]{
                "S.....",
                "---*--",
                "...|..",
                "...|.H"}
        );
        assertThat(got).isEqualTo(1);
    }

    @Test
    public void example4() {
        final int got = instance.fewestCrossings(new String[]{
                "S.F..",
                "..F..",
                "--*--",
                "..|..",
                "..|.H"}
        );
        assertThat(got).isEqualTo(2);
    }

    @Test
    public void example5() {
        final int got = instance.fewestCrossings(new String[]{
                "H|.|.|.|.|.|.|.|.|.|.|.|.|.",
                "F|F|F|F|F|F|F|F|F|F|F|F|F|-",
                "S|.|.|.|.|.|.|.|.|.|.|.|.|."}
        );
        assertThat(got).isEqualTo(27);
    }

    @Test
    public void example6() {
        final int got = instance.fewestCrossings(new String[]{
                "S-H"}
        );
        assertThat(got).isEqualTo(-1);
    }


}
