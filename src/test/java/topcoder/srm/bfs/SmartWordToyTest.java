package topcoder.srm.bfs;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.regex.Pattern;

import static org.fest.assertions.Assertions.assertThat;
import static topcoder.srm.bfs.SmartWordToy.QueueItem;

/**
 * Created by Simon Leghissa on 1/26/14.
 */
public class SmartWordToyTest {

    final SmartWordToy instance = new SmartWordToy();

    @Test
    public void nextWord() throws InvocationTargetException, IllegalAccessException {
        final String got = instance.nextWord("abcd", 0, true);
        assertThat(got).isEqualTo("bbcd");
    }

    @Test
    public void nextWordAfterZ() throws InvocationTargetException, IllegalAccessException {
        final String got = instance.nextWord("abcz", 3, true);
        assertThat(got).isEqualTo("abca");
    }

    @Test
    public void previousWord() throws InvocationTargetException, IllegalAccessException {
        final String got = instance.nextWord("abcd", 0, false);
        assertThat(got).isEqualTo("zbcd");
    }

    @Test
    public void nextWords() throws InvocationTargetException, IllegalAccessException {
        final Collection<QueueItem> got = instance.createNextWords(new QueueItem("ab", 1), Pattern.compile(""));
        assertThat(got).containsOnly(
                new QueueItem("bb", 2)
                , new QueueItem("zb", 2)
                , new QueueItem("ac", 2)
                , new QueueItem("aa", 2)
        );
    }

    @Test
    public void nextWordsWithForbidden() throws InvocationTargetException, IllegalAccessException {
        final Collection<QueueItem> got = instance.createNextWords(new QueueItem("ab", 1), instance.createPattern(new String[]{"bz b"}));
        assertThat(got).containsOnly(
                new QueueItem("ac", 2)
                , new QueueItem("aa", 2)
        );
    }

    @Test
    public void example0() {
        final int got = instance.minPresses("aaaa",
                "zzzz",
                new String[]{"a a a z", "a a z a", "a z a a", "z a a a", "a z z z", "z a z z", "z z a z", "z z z a"});
        assertThat(got).isEqualTo(8);
    }

    @Test
    public void example1() {
        final int got = instance.minPresses("aaaa",
                "bbbb",
                new String[]{});
        assertThat(got).isEqualTo(4);
    }

    @Test
    public void example2() {
        final int got = instance.minPresses("aaaa",
                "mmnn",
                new String[]{});
        assertThat(got).isEqualTo(50);
    }

    @Test
    public void example3() {
        final int got = instance.minPresses("aaaa",
                "zzzz",
                new String[]{"bz a a a", "a bz a a", "a a bz a", "a a a bz"});
        assertThat(got).isEqualTo(-1);
    }

    @Test
    public void example4() {
        final int got = instance.minPresses("aaaa",
                "zzzz",
                new String[]{"cdefghijklmnopqrstuvwxyz a a a",
                        "a cdefghijklmnopqrstuvwxyz a a",
                        "a a cdefghijklmnopqrstuvwxyz a",
                        "a a a cdefghijklmnopqrstuvwxyz"});
        assertThat(got).isEqualTo(6);
    }

    @Test
    public void example5() {
        final int got = instance.minPresses("aaaa",
                "bbbb",
                new String[]{"b b b b"});
        assertThat(got).isEqualTo(-1);
    }

    @Test
    public void example6() {
        final int got = instance.minPresses("zzzz",
                "aaaa",
                new String[]{"abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk",
                        "abcdefghijkl abcdefghijkl abcdefghijkl abcdefghijk"});
        assertThat(got).isEqualTo(-1);
    }
}
