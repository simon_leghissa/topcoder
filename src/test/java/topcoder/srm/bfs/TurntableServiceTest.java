package topcoder.srm.bfs;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 2/1/14.
 */
public class TurntableServiceTest {

    final TurntableService instance = new TurntableService();

    @Test
    public void example0() {
        final int got = instance.calculateTime(new String[]
                {"0 2", "1", "0 1"}
        );
        assertThat(got).isEqualTo(32);
    }

    @Test
    public void example1() {
        final int got = instance.calculateTime(new String[]
                {"0", "0", "0"}
        );
        assertThat(got).isEqualTo(49);
    }

    @Test
    public void example2() {
        final int got = instance.calculateTime(new String[]
                {"4", "1", "2", "3", "0"}
        );
        assertThat(got).isEqualTo(50);
    }

    @Test
    public void example3() {
        final int got = instance.calculateTime(new String[]
                {"0 004", "2 3", "0 01", "1 2 3 4", "1 1"}
        );
        assertThat(got).isEqualTo(35);
    }

}
