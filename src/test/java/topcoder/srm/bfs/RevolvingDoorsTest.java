package topcoder.srm.bfs;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 1/28/14.
 */
public class RevolvingDoorsTest {

    @Test
    public void example0() {
        final int got = new RevolvingDoors().turns(new String[]{"    ### ",
                "    #E# ",
                "   ## # ",
                "####  ##",
                "# S -O-#",
                "# ###  #",
                "#      #",
                "########"});
        assertThat(got).isEqualTo(2);
    }

    @Test
    public void example1() {
        final int got = new RevolvingDoors().turns(new String[]{"#### ",
                "#S|##",
                "# O #",
                "##|E#",
                " ####"});
        assertThat(got).isEqualTo(-1);
    }

    @Test
    public void example2() {
        final int got = new RevolvingDoors().turns(new String[]{
                " |  |  |     |  |  |  |  |  | ",
                " O  O EO -O- O  O  O  O  OS O ",
                " |  |  |     |  |  |  |  |  | "});
        assertThat(got).isEqualTo(7);
    }

    @Test
    public void example3() {
        final int got = new RevolvingDoors().turns(new String[]{"###########",
                "#    #    #",
                "#  S | E  #",
                "#    O    #",
                "#    |    #",
                "#         #",
                "###########"});
        assertThat(got).isEqualTo(0);
    }

    @Test
    public void example4() {
        final int got = new RevolvingDoors().turns(new String[]{
                "        E",
                "    |    ",
                "    O    ",
                "    |    ",
                " -O-S-O- ",
                "    |    ",
                "    O    ",
                "    |    ",
                "         "});
        assertThat(got).isEqualTo(-1);
    }

    @Test
    public void example5() {
        final int got = new RevolvingDoors().turns(new String[]{
                "##E#   ",
                "#  ##  ",
                " -O-## ",
                " #  ## ",
                " ##  ##",
                "  -O-  ",
                "##  ## ",
                " # ### ",
                " #  S  "});
        assertThat(got).isEqualTo(5);
    }

    @Test
    public void example6() {
        final int got = new RevolvingDoors().turns(new String[]{"#############",
                "#  #|##|#   #",
                "#   O  O    #",
                "# E || || S #",
                "#    O  O   #",
                "#   #|##|#  #",
                "#############"});
        assertThat(got).isEqualTo(4);
    }

}
