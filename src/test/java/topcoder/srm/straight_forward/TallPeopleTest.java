package topcoder.srm.straight_forward;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 1/26/14.
 */
public class TallPeopleTest {

    final TallPeople instance = new TallPeople();

    @Test
    public void singleRowSingleColumn() {
        final int[] got = instance.getPeople(new String[]{"2"});
        assertThat(got).isEqualTo(new int[]{2, 2});
    }

    @Test
    public void singleRowMultipleColumn() {
        final int[] got = instance.getPeople(new String[]{"2 3 1"});
        assertThat(got).isEqualTo(new int[]{1, 1});
    }

    @Test
    public void multipleRowMultipleColumn() {
        final int[] got = instance.getPeople(new String[]{"2 3 1", "3 7 5"});
        assertThat(got).isEqualTo(new int[]{3, 3});
    }

    @Test
    public void example0() {
        final int[] got = instance.getPeople(new String[]{"9 2 3",
                "4 8 7"});
        assertThat(got).isEqualTo(new int[]{4, 7});
    }

    @Test
    public void example1() {
        final int[] got = instance.getPeople(new String[]{"1 2",
                "4 5",
                "3 6"});
        assertThat(got).isEqualTo(new int[]{4, 4});
    }

    @Test
    public void example2() {
        final int[] got = instance.getPeople(new String[]{"1 1",
                "1 1"});
        assertThat(got).isEqualTo(new int[]{1, 1});
    }
}
