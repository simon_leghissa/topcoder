package topcoder.srm.straight_forward;


import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Leghissa Simon on 1/25/14.
 */
public class BusinessTasksTest {


    final BusinessTasks instance = new BusinessTasks();

    @Test
    public void listWithOneElement() {
        final String got = instance.getTask(new String[]{"onlyTask"}, 10000000);
        assertThat(got).isEqualTo("onlyTask");
    }

    @Test
    public void example0() {
        final String got = instance.getTask(new String[]{"a", "b", "c", "d"}, 2);
        assertThat(got).isEqualTo("a");
    }

    @Test
    public void example1() {
        final String got = instance.getTask(new String[]{"a", "b", "c", "d", "e"}, 3);
        assertThat(got).isEqualTo("d");
    }

    @Test
    public void example2() {
        final String got = instance.getTask(new String[]{"alpha", "beta", "gamma", "delta", "epsilon"}, 1);
        assertThat(got).isEqualTo("epsilon");
    }

    @Test
    public void example3() {
        final String got = instance.getTask(new String[]{"a", "b"}, 1000);
        assertThat(got).isEqualTo("a");
    }

    @Test
    public void example4() {
        final String got = instance.getTask(new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
                "u", "v", "w", "x", "y", "z"}, 17);
        assertThat(got).isEqualTo("n");
    }

    @Test
    public void example5() {
        final String got = instance.getTask(new String[]{"zlqamum", "yjsrpybmq", "tjllfea", "fxjqzznvg", "nvhekxr", "am", "skmazcey", "piklp",
                "olcqvhg", "dnpo", "bhcfc", "y", "h", "fj", "bjeoaxglt", "oafduixsz", "kmtbaxu",
                "qgcxjbfx", "my", "mlhy", "bt", "bo", "q"}, 9000000);
        assertThat(got).isEqualTo("fxjqzznvg");
    }
}
