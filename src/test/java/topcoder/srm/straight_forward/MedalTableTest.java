package topcoder.srm.straight_forward;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Simon Leghissa on 1/26/14.
 */
public class MedalTableTest {

    final MedalTable instance = new MedalTable();

    @Test
    public void singleCountry() {
        final String[] got = instance.generate(new String[]{"ITA ITA ITA", "ITA ITA ITA", "ITA ITA ITA"});
        assertThat(got).isEqualTo(new String[]{"ITA 3 3 3"});
    }

    @Test
    public void twoCountries() {
        final String[] got = instance.generate(new String[]{"ITA BRA ITA", "BRA ITA ITA", "ITA ITA ITA"});
        assertThat(got).isEqualTo(new String[]{"ITA 2 2 3", "BRA 1 1 0"});
    }

    @Test
    public void orderingByGoldMedals() {
        final String[] got = instance.generate(new String[]{"ITA BRA BRA", "ITA ITA ITA"});
        assertThat(got).isEqualTo(new String[]{"ITA 2 1 1", "BRA 0 1 1"});
    }

    @Test
    public void orderingBySilverMedals() {
        final String[] got = instance.generate(new String[]{"BRA ITA BRA", "ITA ITA ITA"});
        assertThat(got).isEqualTo(new String[]{"ITA 1 2 1", "BRA 1 0 1"});
    }

    @Test
    public void orderingByBronzeMedals() {
        final String[] got = instance.generate(new String[]{"BRA BRA ITA", "ITA ITA ITA"});
        assertThat(got).isEqualTo(new String[]{"ITA 1 1 2", "BRA 1 1 0"});
    }

    @Test
    public void orderingByName() {
        final String[] got = instance.generate(new String[]{"BRA BRA BRA", "ITA ITA ITA"});
        assertThat(got).isEqualTo(new String[]{"BRA 1 1 1", "ITA 1 1 1"});
    }

    @Test
    public void example0() {
        final String[] got = instance.generate(new String[]{"ITA JPN AUS", "KOR TPE UKR", "KOR KOR GBR", "KOR CHN TPE"});
        assertThat(got).isEqualTo(new String[]{"KOR 3 1 0",
                "ITA 1 0 0",
                "TPE 0 1 1",
                "CHN 0 1 0",
                "JPN 0 1 0",
                "AUS 0 0 1",
                "GBR 0 0 1",
                "UKR 0 0 1"});
    }

    @Test
    public void example1() {
        final String[] got = instance.generate(new String[]{"USA AUT ROM"});
        assertThat(got).isEqualTo(new String[]{"USA 1 0 0", "AUT 0 1 0", "ROM 0 0 1"});
    }

    @Test
    public void example2() {
        final String[] got = instance.generate(new String[]{"GER AUT SUI", "AUT SUI GER", "SUI GER AUT"});
        assertThat(got).isEqualTo(new String[]{"AUT 1 1 1", "GER 1 1 1", "SUI 1 1 1"});
    }
}
